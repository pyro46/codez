#!/usr/bin/env python
import stat
import errno
import fuse
from time import time
from subprocess import *
from syslog import * 
import os 
import util 
import thread 
import sys 

fuse.fuse_python_api = (0, 2)

fuse.feature_assert('stateful_files', 'has_init')

install_str = "#!/bin/bash\n#This file is created using aptFS.py\nsudo apt-get install "

class MyStat(fuse.Stat):
	def __init__(self):
		self.st_mode = 0 #stat.S_IFDIR | 0755
		self.st_ino = 0
		self.st_dev = 0
		self.st_nlink = 0
		self.st_uid = os.getuid()
		self.st_gid = os.getgid()
		self.st_size = 0 #4096
		self.st_atime = time()
		self.st_mtime = self.st_atime
		self.st_ctime = self.st_atime 

class aptFS(fuse.Fuse):
	def __init__(self, *args, **kw):
		fuse.Fuse.__init__(self, *args, **kw)

		openlog('aptFS',0,LOG_DAEMON)
		self.printers = {}
		self.root = "/var/cache/apt/archives"

	def mythread(self):
		self.log("I am in thread %s"%(self.root)) 
	#	self.log(self.printers)
		self.printers = util.scanDir(self.root)

	def fsinit(self):
		self.log("fsinit")
		thread.start_new_thread(self.mythread,())
		#self.printers = util.checkDependency(self.root)
		#self.printers = util.scanDir(self.root)

	def log(self,msg):
		syslog(LOG_NOTICE,msg)

	def getattr(self, path):
		self.log("geattr : %s" %(path))
		st = MyStat()
		pe = path.split('/')[1:]
		st.st_atime = int(time())
		st.st_mtime = st.st_atime
		st.st_ctime = st.st_atime
		
		if path == '/':
			self.log("in root")
			st.st_mode = stat.S_IFDIR | 0755
			st.st_nlink = 2 
			pass
		elif path == "/" + pe[-1]:
			st.st_mode = stat.S_IFDIR | 0755
			st.st_nlink = 2
		elif self.printers.has_key(pe[-1]) and path != "/" + pe[-1]:
			self.log("handle symlinks %s"%(pe[-1]))
			st.st_mode = stat.S_IFLNK | 0777 
			st.st_nlink = 2 
		elif [True for x in self.printers.keys() if pe[-1] in self.printers[x]]:
			self.log("deb file link is " + pe[-1])
			if os.path.splitext(pe[-1])[-1] == ".deb":
				st.st_mode = stat.S_IFLNK | 0777 
				st.st_nlink = 2 
			else:
				st.st_mode = stat.S_IFREG | 0555 
				st.st_nlink = 1
				st.st_size = len(install_str + pe[-1] + "\n")
		else:
			self.log("What is this ? %s %s len %s" %(path,pe[-1],len(pe)))
			self.log("path is now %s"%(os.path.splitext(pe[-1])[-1]))
			if os.path.splitext(pe[-1])[-1] == ".deb":
				st.st_mode = stat.S_IFLNK | 0777 
				st.st_nlink = 2 
			else:
				return -errno.ENOENT
		return st
	
	def readdir(self, path, offset):
		dirents = [ '.', '..' ]
		if path == '/':
			dirents.extend(self.printers.keys())
		else:
			for each in self.printers[path[1:]]:
				if os.path.splitext(each)[-1] == ".deb":
					dirents.append(os.path.basename(each))
				else:
					dirents.append(each)
		self.log("Dirents path %s contains %s" %(path,dirents))
		
		for r in dirents:
			yield fuse.Direntry(r)
	
	def mknod(self, path, mode, dev):
		self.log("mknod path %s"%(path))
		pe = path.split('/')[1:]
		if len(pe) == 1 :
			self.printers[pe[0]] = []
		return 0
	
	def mkdir(self, path, mode):
		self.log("mkdir %s"%(path))
		pe = path.split('/')[1:]
		if len(pe) == 1:
			self.printers[pe[0]] = []
		self.log(self.printers)
		return 0

	def rmdir(self, path):
		pe = path.split('/')[1:]
		if len(pe) == 1:
			del(self.printers[pe[0]])
		return 0
	
	def read(self, path, size, offset):
		pe = path.split('/')[1:]
		new_install_str = install_str + pe[-1] + "\n"
		slen = len(new_install_str)
		self.log("new install str %s %s"%(slen,new_install_str))
		if offset < slen :
			if offset + size > slen:
				size = slen - offset 
			buf = new_install_str[offset:offset+size]
		else:
			buf = ""
		
		return buf
	
	def open(self, path, flags):
		accmode = os.O_RDONLY | os.O_WRONLY | os.O_RDWR
		if (flags & accmode) != os.O_RDONLY:
			return -errno.EACCES

	def truncate(self, path, size):
		return 0
	def utime(self, path, times):
		return 0
	def rename(self, pathfrom, pathto):
		return 0
	def fsync(self, path, isfsyncfile):
		return 0

	def readlink(self,path):
		self.log("readlink %s"%(path))
		pe = path.split("/")[-1]
		if os.path.splitext(pe)[-1] == ".deb":
			#newpath = self.root + "/" + pe 
			self.log("self.printers %s"%(self.printers[path.split("/")[1:][0]][0]))
			newpath = self.printers[path.split("/")[1:][0]][0]
			#newpath = self.printers[pe][0]
			#self.log("new path can be %s "%(self.printers[pe][0]))
		else:
			newpath = "../" + pe 
		return newpath
		
def main():
	usage=""" aptFS: A Python-Fuse based fs which allows to mount debian package dependency script \n\n"""+ fuse.Fuse.fusage
	server = aptFS(version="%prog " + fuse.__version__,usage=usage, dash_s_do='setsingle')
	server.parser.add_option(mountopt="root",metavar="PATH",default="/var/cache/apt/archives",help="default path of package cache [default= %default]")
	server.parse(values=server,errex=1)

	try:
		server.main()
	except fuse.FuseError,e:
		sys.exit("Error in mounting")

if __name__ == '__main__':
	main()

