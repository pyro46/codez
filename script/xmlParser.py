#!/usr/bin/python 
from xml.dom.minidom import parse 

class XMLParser():
	def __init__(self,fileName):
		self.list_directories = {} 
		self.list_files = {} 

		self.fileContent = parse(fileName)

	def nodeToDic(self,return_file=0):
		for n in self.fileContent.childNodes:
			for each in n.childNodes:
				if each.nodeType == each.TEXT_NODE:
					continue
				elif each.nodeType == each.ELEMENT_NODE:
					if each.getAttribute('is_dir') == '0':
						self.list_files[str(each.nodeName)] = str(self.handleText(each))
					else:
						self.list_directories[str(each.nodeName)] = str(self.handleChild(each))

					
		if return_file == 0:
			return self.list_directories
		elif return_file == 1:
			return self.list_files

	def handleChild(self,node):
		list_child = [] 
		for n in node.childNodes:
			if n.nodeType != n.TEXT_NODE:
				list_child.append(str(n.nodeName))
				if n.getAttribute('is_dir') == '0':
					self.list_files[str(n.nodeName)] = self.handleText(n)
		return str(list_child)

	def handleText(self,node):
		text = "" 
		for n in node.childNodes:
			if n.nodeType == n.TEXT_NODE:
				text += str(n.nodeValue)
			else:
				pass
		return text 

xmlParser = XMLParser('tree.xml')
print xmlParser.nodeToDic(1)
print xmlParser.nodeToDic(0)
