import Image 
import sys 

im = Image.open(sys.argv[1])

immode = {'1':'1 Bit', 'L':'8Bit and BW', 'P': '8 Bit with Color', 'RGB':'3x8-bit pixels, true colour', 'RGBA':'4x8-bit pixels, true colour with transparency mask','CMYK':'(4x8-bit pixels, colour separation)','YCbCr':' (3x8-bit pixels, colour video format)','I':'(32-bit signed integer pixels)','F':'(32-bit floating point pixels)'}

print immode[im.mode]

