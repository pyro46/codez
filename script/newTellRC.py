#!/usr/bin/python 
from __future__ import print_function 
import glob 
import os,sys
from optparse import OptionParser,SUPPRESS_HELP

rls = [0,1,2,3,4,5,6,'S'] 

class MyService():
	def __init__(self):
		self.baseInitDir = "/etc/init.d"

	def getServiceInfo(self,service='halt'):
		tab = {}
		ServiceList = []
		for level in rls:
			runlevelList = [row[3:] for row in os.listdir("/etc/rc%s.d"%(level))]
			if service in runlevelList:
				ServiceList.append('Yes')
			else:
				ServiceList.append('No')
		tab[service] = ServiceList
		return tab 

	def main(self,service='all'):
		listFile = []
		if service == 'all':
			listFile = os.listdir(self.baseInitDir)
		else:
			if service in os.listdir(self.baseInitDir):
				listFile = [service]
			else:
				sys.exit("No service named \"%s\" found" %(service))

		maxLen = max([len(temp) for temp in listFile])
		print("Service".ljust(maxLen) + "\t  R0" +"\t  R1" + "\t  R2" + "\t  R3" + "\t  R4" + "\t  R5" + "\t  R6" + "\t   S")
		for each in listFile:
			Service = self.getServiceInfo(each)
			for i in range(0,len(Service.keys())):
				print(Service.keys()[i].ljust(maxLen),end='\t')
				for j in range(0,len(Service[Service.keys()[i]])):
					print(" ",Service[Service.keys()[i]][j],end='\t')
			print(end='\n')
def main():
	S = MyService()
	parser = OptionParser(usage="%prog [options]",version="%prog 1.0",add_help_option=False)
	parser.add_option('-h','--help',action='help',default=True)
	parser.add_option('-a','--all',dest='allservice',action='store',nargs=0,help="Print status of all service")
	parser.add_option('-s','--service',dest='service',action='store',nargs=1,help="Specify particular name of service")
	options, args = parser.parse_args() 

	#print(options)
	#print(args)
	if not options.allservice == None and len(options.allservice) == 0:
		if len(args) > 0:
			print("Error: -a/--all takes no argument")
		else:
			S.main()
	if options.service:
		S.main(options.service)

if __name__ == "__main__":
	main() 
