def last_unique(iter):
    '''
    Like unique, but prefer items toward the end of the list.  Also, returns
    a list rather than an iterator object.

    Given a list l, last_unique(l) is functionally equivalent to
    list(reversed(list(unique(reversed(l))))).

    >>> last_unique([1, 2, 5, 4, 5, 2, 3, 6, 2])
    [1, 4, 5, 3, 6, 2]
    '''
    result = []
    for item in iter:
        try:
            result.remove(item)
        except ValueError:
            pass
        result.append(item)
    return result

