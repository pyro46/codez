import dbus

T_SERVICE_NAME = "org.mpris.Totem"
T_OBJECT_PATH = "/Player"
T_INTERFACE = "org.freedesktop.MediaPlayer"

session_bus= dbus.SessionBus()

totem = session_bus.get_object(T_SERVICE_NAME, T_OBJECT_PATH)
totem_mediaplayer = dbus.Interface(totem, dbus_interface=T_INTERFACE)

print totem_mediaplayer.GetMetadata()
print totem_mediaplayer.PositionGet()
#totem_mediaplayer.Play()
totem_mediaplayer.Pause()
totem_mediaplayer.VolumeSet(50)

# query dbus service using 
# qdbus org.mpris.Totem /Player
# qdbus org.mpris.Totem /Player org.freedesktop.MediaPlayer.GetMetaData
