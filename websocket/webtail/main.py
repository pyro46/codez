import tornado.httpserver 
import tornado.web 
import tornado.ioloop 
import tornado.websocket
import os 

HOST= 'localhost'
PORT= 9999 
LISTENERS=[]

class TailReaderHandler(tornado.websocket.WebSocketHandler):
	def open(self):
        	print "WebSocket open", self
		LISTENERS.append(self)

	def on_message(self, message):
		print "Websocket received ", message
		pass

	def on_close(self):
	        print "WebSocket close"
		try:
			LISTENERS.remove(self)
		except:
			pass


class TailHandler(tornado.web.RequestHandler):
	def post(self):
		logfilename = self.get_argument("logfile")		
		logfilename1 = logfilename[1:]
		self.render("tail.html",logfile=logfilename1)
		call(logfilename)

def call(filename):		
	tailed_file = open(filename)
	tailed_file.seek(os.path.getsize(filename))

	def check_file():
		where = tailed_file.tell()
		line = tailed_file.readline()
		if not line:
			tailed_file.seek(where)
		else:
			print "File refresh"
			for element in LISTENERS:
				element.write_message(line)

	tailed_callback = tornado.ioloop.PeriodicCallback(check_file, 100)
	tailed_callback.start()


class MainHandler(tornado.web.RequestHandler):
	def get(self):
		self.render("index.html") 

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/',MainHandler),
	    (r'/tail',TailHandler),
	    (r'/tailreader/',TailReaderHandler)
        ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
        )
        tornado.web.Application.__init__(self, handlers, **settings)
	
def main():
	application = Application()
	http_server = tornado.httpserver.HTTPServer(application) 
	http_server.listen(PORT)
	io_loop = tornado.ioloop.IOLoop.instance()
	print "Starting webtail at ", HOST, PORT

	try:
		io_loop.start() 
	except SystemError, KeyboardInterrupt:
		io_loop.stop()

if __name__ == '__main__':
	try:
		main() 
	except:
		pass 
