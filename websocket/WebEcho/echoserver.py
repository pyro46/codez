import time 
from geventwebsocket.handler import WebSocketHandler 
from gevent import pywsgi 
import gevent 

def handle(ws):
	"""
	This is websocket handler function.
	"""
	if ws.path == '/echo':
		while True:
			m = ws.receive()
			if m is None:
				break
			print "Received from websocket :",m
			ws.send(m)
			print "Sending to websocket :",m

def app(environ, start_response):
	"""
	Route handler Currently handling only /echo 
	"""
	if environ['PATH_INFO'] in ('/echo'):
		handle(environ['wsgi.websocket'])
	else:
		print "404 Not Found"
		start_response("404 Not Found", [])
		return []


def main():
	print "In main, Starting WSGIServer"
	server = pywsgi.WSGIServer(('0.0.0.0', 8000), app,
			handler_class=WebSocketHandler)
	try:
		server.serve_forever() 
	except:
		pass

if __name__ == "__main__":
	main() 

