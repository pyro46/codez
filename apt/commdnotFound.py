def _getSourcesList():
	try:
		import apt_pkg
		from aptsources.sourceslist import SourcesList
		apt_pkg.init()
	except (SystemError, ImportError), e:
		return []
	sources_list = set([])
	for source in SourcesList():
		if not source.disabled and not source.invalid:
			for component in source.comps:
				sources_list.add(component)
	return sources_list


print _getSourcesList() 
