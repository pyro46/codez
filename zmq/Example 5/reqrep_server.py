import zmq 
import time 
import sys 

PORT = "5556" 

if len(sys.argv) > 1:
	PORT = int(sys.argv[1])

#server is created with a scoket type zmq.REP and is bound to well port 
context = zmq.Context()
socket = context.socket(zmq.REP) 
socket.bind("tcp://*:%s" % PORT)

#block on recv() to get a request before it can send a reply 

while True:
	message = socket.recv() 
	print "Received request : ", message
	time.sleep(1)
	socket.send("World from %s" % PORT)
