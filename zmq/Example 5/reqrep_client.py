import zmq 
import time 
import sys 

PORT = "5556" 

if len(sys.argv) > 2:
	PORT1 = int(sys.argv[2])
elif len(sys.argv) > 1:
	PORT = int(sys.argv[1])

#Client is created with a socket type "zmq.REQ"	
context = zmq.Context()
socket = context.socket(zmq.REQ) 

socket.connect("tcp://localhost:%s" % PORT)
if len(sys.argv) > 2:
	socket.connect("tcp://localhost:%s" % PORT1)

#send 10 request 
for request in range(1,10):
	print "Sending request ", request, "..." 
	socket.send("hello")
	message = socket.recv()
	print "Received reply", request, "[", message ,"]"
