import zmq 
import random 
import sys 
import time 

PORT = "5556" 

context = zmq.Context() 

socket = context.socket(zmq.PAIR)
socket.connect("tcp://localhost:%s" %PORT)

while True:
	msg = socket.recv()
	print msg 
	socket.send("client message to server1")
	socket.send("client message to server2") 
	time.sleep(1)
