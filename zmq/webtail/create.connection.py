from pymongo import Connection 
#create connection to mongod 
#connection = Connection() # if mongod is running on default port 27017 
print "Starting connection"
connection = Connection('localhost',27017)

print "getting database" 
#getting database 
#db = connection['foo'] if database name contains a '-'
db = connection['foo']

print "creating collection"
collection = db.bar

print "inserting data in collection"
collection.insert({'a':1})

print "The foo database contains these collections ", db.collection_names()

print "This the first document in bar collection in foo database ", collection.find_one()

for c in collection.find():
	print c

print "The collection contains", collection.count() , " documents"
