import zmq 
import sys 
from pymongo import Connection 

PORT = "5511" 

context = zmq.Context() 
socket = context.socket(zmq.PULL) 

#create connection to mongod 
#connection = Connection() # if mongod is running on default port 27017 
print "Starting connection"
connection = Connection('localhost',27017)

print "Collecting updates from server ..." 
socket.connect("tcp://localhost:%s" %PORT)

print "getting database" 
#getting database 
#db = connection['foo'] if database name contains a '-'
db = connection['foo']

print "creating collection"
collection = db.bar

while True:
	try:
		string = socket.recv() 
		collection.insert({'log':string})
	except:
		print "Error occured "
	print string , 'added to database'
