import zmq 
from random import choice 
import time 
import json 
import math 

context = zmq.Context() 
socket = context.socket(zmq.PUB) 

socket.bind("tcp://127.0.0.1:5000") 

while True:
	x = time.time() * 1000 
	y = 2.5 * (1 + math.sin(x / 500)) 
	socket.send(json.dumps(dict(x=x,y=y)))
