!/usr/bin/env python
import fuse
from fuse import Fuse
import os
from errno import *
from stat import *
import sys
import re
import glob
import types 

fuse.fuse_python_api = (0, 2)

index_dir="/media/du/mp3"
logfile='/home/abhijeet/fs.log'

def stripnulls(data):
	"strip whitespace and nulls"
        return data.replace("\00", "").strip()

class Psfs(Fuse):
	#flags = 1
	#index_var = 0;
	
	def __init__(self, *args, **kw):
		self.root = index_dir
		self.log('Starting with ' + self.root)
		Fuse.__init__(self, *args, **kw)
	
	def p(self,path):
		return index_dir+path

	def log(self,message):
		l = open(logfile,'a')
		l.write(message+'\n')
		l.close()
		pass
	
	def getattr(self, path):
		#getattr 
		self.log("getattr " + self.root + path)
		#self.log("getattr self.root=" + self.root)
		return os.lstat(self.root + path)

	def readlink(self, path):
		#readlink - this is useful to follow symlinks 
		self.log("readlink " + self.root + path)
		return os.readlink(self.root + path)

	#def getdir(self, path):
	#	#self.log("getdir " + path)
	#	return map(lambda x: (x,0), os.listdir(path))

	def unlink(self, path):
		# unlink - this function is useful for removing symlinks 
		self.log("unlink " + self.root + path)
		return os.unlink(self.root + path)

	def readdir(self, path, offset):
		self.log('listing ' + self.root + path)
		for e in os.listdir(self.root + path):
			self.log('yielding path: ' + self.root + path)
			yield fuse.Direntry(e)

	def rmdir(self, path):
		self.log("rmdir " + self.root + path)
		return os.rmdir(self.root + path))
	
	def symlink(self, path, path1):
		if not path.startswith('/'):
			path = '/' + path
		self.log("symlink %s %s" % (path,path1))
		return os.symlink(self.root + path , self.root +  path1)
	
	def rename(self, path, path1):
		self.log("rename %s %s" % (self.root + path, self.root + path1))
		return os.symlink(self.root + path, self.root + path1)
	
	def link(self, path, path1):
		self.log("link %s %s" % (self.root + path,self.root + path1))
		return os.link(self.root + path, self.root +path1)

	def chmod(self, path, mode):
		self.log("chmod %s %s" % (self.root + path,mode))
		return os.chmod(self.root + path, mode)

	def chown(self, path, user, group):
		self.log("chown %s %s %s" % (self.root + path,user,group))
		return os.chown(self.root + path, user, group)

	def truncate(self, path, size):
		self.log("truncate %s %s" % (self.root + path,size))
		f = open(self.root + path, "w+")
		aux = f.truncate(size)
		return aux
	
	def mknod(self, path, mode, dev):
	 	""" Python has no os.mknod, so we can only do some things """
		self.log("mknod %s %s %s" %(self.root + path,mode,dev))
		if S_ISREG(mode):
			open(self.root + path, "w")
		else:
			return -EINVAL
	
	def mkdir(self, path, mode):
		self.log("mkdir %s %s" %(self.root + path,mode))
		return os.mkdir(self.root + path, mode)

	def utime(self, path, times):
		self.log("utime %s %s" % (self.root + path,times))
		return os.utime(self.root + path, times)

	def open(self, path, flags):
		self.log("open %s %s" % (self.root + path,flags))
		#os.open(self.p(path), flags)
		return 0

	def read(self, path, len, offset):
		#self.log(str(os.environ))
		self.log("read %s %s %s" %(self.root + path, len, offset))
		f = open(self.root + path, "r")
		#print f.read()
		f.seek(offset)
		return f.read(len)
	
	def write(self, path, buf, off):
		self.log("write %s <buf> %s" % (self.root + path,off))
		f = open(self.root + path, "a")
		f.seek(off)
		f.write(buf)
		length = len (buf)
		return length

if __name__ == '__main__':
	#boilerplate for --help # python %prog --help 
	usage = """ Psfs - a userspace filesystem  for logging activity of usage """ #+ Fuse.fusage 
	server = Psfs(version="%prog " + fuse.__version__,usage=usage,dash_s_do='setsingle')
	# version = 0.2.1 
	# usage = above usage text
	# next line passes the default mount position if server.root or self.root or index_dir is not passed 
	server.parser.add_option(mountopt="root",metavar="PATH", default='/',help="mirror filesystem from under path [default: %default]")
	server.parse(values=server,errex=1)
	try:
		if server.fuse_args.mount_expected():
			os.chdir(server.root)
			print "Filesystem root is " + server.root 
	except OSError:
		print >> sys.stderr, "cant enter root of underlying fs"
		sys.exit(1)
	
	server.main()
