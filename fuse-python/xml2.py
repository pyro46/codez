#!/usr/bin/python 
from xml.dom.minidom import parse 
import os, stat, errno
import fuse
from fuse import Fuse
from syslog import * 
import time 

if not hasattr(fuse, '__version__'):
    raise RuntimeError, \
        "your fuse-py doesn't know of fuse.__version__, probably it's too old."

fuse.fuse_python_api = (0, 2)

hello_path = '/hello'
hello_str = 'Hello World!\n'


class XMLParser():
	def __init__(self,fileName):
		self.list_directories = {} 
		self.list_files = {} 

		self.fileContent = parse(fileName)

	def nodeToDic(self,return_file=0):
		for n in self.fileContent.childNodes:
			for each in n.childNodes:
				if each.nodeType == each.TEXT_NODE:
					continue
				elif each.nodeType == each.ELEMENT_NODE:
					if each.getAttribute('is_dir') == '0':
						self.list_files[each.nodeName] = self.handleText(each)
					else:
						self.list_directories[each.nodeName] = self.handleChild(each)

					
		if return_file == 0:
			return self.list_directories 
		elif return_file == 1:
			return self.list_files

	def handleChild(self,node):
		list_child = [] 
		for n in node.childNodes:
			if n.nodeType != n.TEXT_NODE:
				list_child.append(n.nodeName)
				if n.getAttribute('is_dir') == '0':
					self.list_files[n.nodeName] = self.handleText(n)
		return list_child 

	def handleText(self,node):
		text = "" 
		for n in node.childNodes:
			if n.nodeType == n.TEXT_NODE:
				text += n.nodeValue 
			else:
				pass
		return text 




class MyStat(fuse.Stat):
    def __init__(self):
        self.st_mode = stat.S_IFREG | 0666
        self.st_ino = 0
        self.st_dev = 0
        self.st_nlink = 1
        self.st_uid = os.getuid()
        self.st_gid = os.getgid()
        self.st_size = 4096
        self.st_atime = time.time()
        self.st_mtime = self.st_atime
        self.st_ctime = self.st_atime

class HelloFS(Fuse):
    def __init__(self,list_files,list_directories,*args,**kw):
	    fuse.Fuse.__init__(self,*args,**kw)

	    openlog("HelloFS",0,LOG_DAEMON)

	    self.list_directories = list_directories 
	    self.list_files = list_files 
	    syslog(LOG_NOTICE,"list_directories : %s"%(list_directories))
	    syslog(LOG_NOTICE,"list_files : %s"%(list_files))
	    #self.list_directories = {"Abhijeet":"Abhijeet.txt","Adesh":"None"}
	    #self.list_files = {"Abhijeet.txt":"This is text from abhijeet.txt belongs to Abhijeet directory\n","None":"This is a none\n"}

	    for line in self.list_directories.keys():
		    syslog(LOG_NOTICE,"Directories in list : %s"%(line))

    def getattr(self, path):
	syslog(LOG_NOTICE,"getAttr getting called is %s"%(path.split("/")[-1]))
			
	st = MyStat()
        if path == '/':
            st.st_mode = stat.S_IFDIR | 0755
            st.st_nlink = 2

   	elif path == hello_path:
            st.st_mode = stat.S_IFREG | 0666
            st.st_nlink = 1
            st.st_size = len(hello_str)

	elif path.split("/")[-1] in self.list_files.keys():
		syslog(LOG_NOTICE,"getAttr for file :%s"%(path.split("/")[-1]))
		st.st_mode = stat.S_IFREG | 0666
		st.st_nlink = 1
		st.st_size = len(self.list_files[path.split("/")[-1]])
	    
	elif path.split("/")[-1] in self.list_directories.keys():
		syslog(LOG_NOTICE,"getAttr for directory :%s"%(path.split("/")[-1]))
		st.st_mode = stat.S_IFDIR | 0755 
		st.st_nlink = 2
        else:
		#this is the place you decide how to create file or direcory 
		syslog(LOG_NOTICE,"Unknown type :%s" %(path.split("/")[-1]))
		#return -errno.ENOENT
        return st

    def readdir(self, path, offset):
	    syslog(LOG_NOTICE,"Readdir : Path is :%s"%(path.split("/")[-1]))
	    dirents = ['.','..']
	    if path == "/":
		    dirents.extend(self.list_directories.keys())
	    else:
		    syslog(LOG_NOTICE,"Readdir files :%s" %(self.list_directories[path[1:]]))
		    dirents.append(self.list_directories[path[1:]])
		    
	    for r in dirents:
		    syslog(LOG_NOTICE,"Directory entries :%s"%(r))
		    yield fuse.Direntry(r)

    def open(self, path, flags):
	    syslog(LOG_NOTICE,"opening file %s"%(path.split("/")[-1]))
	    return 0
	    #if path.split("/")[-1] != self.list_files.keys():
	#	    return -errno.ENOENT
	#if path != hello_path:
        #    return -errno.ENOENT
        #accmode = os.O_RDONLY | os.O_WRONLY | os.O_RDWR
        #if (flags & accmode) != os.O_RDONLY:
        #    return -errno.EACCES

    def read(self, path, size, offset):
	    #if path.split("/")[-1] != self.list_files.keys():
	    #	    return -errno.ENOENT
	    syslog(LOG_NOTICE,"Reading file %s" %(path.split("/")[-1]))
	    syslog(LOG_NOTICE,"Length of file %s" %(len(self.list_files[path.split("/")[-1]])))
	    slen = len(self.list_files[path.split("/")[-1]])
	    if offset < slen:
	    	if offset + size > slen:
			size = slen - offset
		buf = self.list_files[path.split("/")[-1]][offset:offset+size]
            else:
            	buf = ''
      	    return buf

    def write(self,path,buf,offset):
	    syslog(LOG_NOTICE,"write: buf=%s offset=%s" %(buf,offset))
	    path = path.split("/")[1:]
	    self.list_files[path[1]] += buf 
	    return len(buf)

    def unlink(self,path):
	    syslog(LOG_NOTICE,"unlink: path=%s"%(path))
	    path = path.split("/")[1:]
	    syslog(LOG_NOTICE,"unlink: path removed %s"%(self.list_files[path[1]]))
	    self.list_directories[path[0]].remove(path[1])
	    del(self.list_files[path[1]])
	    syslog(LOG_NOTICE,"unlink: list is %s"%(self.list_files.keys()))
	    return 0

    def utime(self,path,times):
	    path = path.split("/")[1:]
	    syslog(LOG_NOTICE,"utime: path=%s %s"%(path,times))
	    if path[1] in self.list_files.keys():
	  	  os.utime(path[1],times)
	    else:
		  syslog(LOG_NOTICE,"utime")
		  self.list_directories[path[0]].append(path[1])
		  self.list_files[path[1]] = ""
		  os.utime(path[1],times)
		  return 0


    def mknod(self,path,mode,dev):
	    path = path.split("/")[1:]
	    syslog(LOG_NOTICE,"mknod : path=%s,%s  mode=%s dev=%s" %(path[0],path[1],mode,dev))
	    self.list_directories[path[0]].append(path[1])
	    self.list_files[path[1]] = ""
	    syslog(LOG_NOTICE,"mknod : list_directories=%s\nlist_files=%s"%(self.list_directories,list_files))

	    return 0 


def main():
	xmlParser = XMLParser('tree.xml')
	list_files = xmlParser.nodeToDic(1)
	list_directories = xmlParser.nodeToDic(0)
	usage=""" Userspace hello example """ + Fuse.fusage
	server = HelloFS(list_files,list_directories,version="%prog " + fuse.__version__,usage=usage)
	server.parse(errex=1)
	server.main()

if __name__ == '__main__':
    main()
