from bottle import route, run 

@route('/hello')
def hello():
	return "Hello World from python-bottle"

run(host='localhost',port=12345)
