from bottle import get,post,request,run , error  

@get('/login')
def login_form():
	return '''
	<form method="POST">
		<p>Name : <input name="name" type="text" /><br/>
		<p>Password <input name="password" type="password"/><br/>
		<p><input type="submit" name="submit"/>'''

@post('/login')
def login_submit():
	name = request.forms.get('name')
	password = request.forms.get('password') 
	
	if name and password:
		return "<p> Your login was correct</p>"
	else:
		return "<p>Login Failed</p>"

@error(404)
def error404(error):
	return "Nothing here, sorry " 

run(host='localhost',port=12345)
