from bottle import route, request, response,run 

@route('/') 
def counter():
	count = int(request.COOKIES.get('counter','0'))
	count += 1 
	response.set_cookie('counter',str(count))
	return "You visited this page %d pages" %count 

run(host='localhost',port=12345)

