import gtk
def yesNoDialog(yes_text="Yes", no_text="No", cancel_text="Cancel"):
	'''
	returns 1 if yes was pressed,
	        0 if no  was pressed,
           -1 if dialog was cancled
		   '''
	
	d=gtk.GtkWindow()
	hbox = gtk.GtkHButtonBox()
	def delete_event(widget, event, d):
		d.callback_return=-1
		return gtk.FALSE
	d.connect("delete_event", delete_event, d)
	d.add(hbox)
	def callback(widget, data):
		d=data[0]
		data=data[1]
		d.hide()
		d.callback_return=data

	yes = gtk.GtkButton(yes_text)
	yes.connect("clicked", callback, (d, 1))
	hbox.pack_start(yes)

	no = gtk.GtkButton(no_text)
	no.connect("clicked", callback, (d, 0))
	hbox.pack_start(no)

	cancel = gtk.GtkButton(cancel_text)
	cancel.connect("clicked", callback, (d, -1))
	hbox.pack_start(cancel)

	d.set_modal(gtk.TRUE)
	d.show_all()
	d.callback_return=None
	while d.callback_return==None:
		gtk.mainiteration(gtk.TRUE) # block until event occurs
	return d.callback_return

yesNoDialog()
