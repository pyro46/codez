import gtk 

dialog = gtk.Dialog("Are you sure?", None, 0,(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_NO, gtk.RESPONSE_NO, gtk.STOCK_YES, gtk.RESPONSE_YES))

label = gtk.Label("Are you sure ?")
label.show() 
dialog.get_content_area().add(label)

response = dialog.run()

if response == gtk.RESPONSE_YES:
	print "Yes"
elif response == gtk.RESPONSE_NO:
	print "No"
else:
	print "Error"
