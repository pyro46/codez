#!/usr/bin/python 

# quiklist v1.0
# Instructions

# 1. Copy file quiklist.py (this file) to /usr/local/bin
# 2. Create or edit file .quiklist in home directory to add one command per line
# 3. You can if you wish add additional command options. Make sure command is correctly spelled
# 4. Launch quiklist.py
# 5. Or better yet, add it to gnome startup programs list so it's run on login.

# v1.0 - Initial push

import gobject 
import gtk 
import appindicator 
from subprocess import Popen 
import sys 
import os 

ver = 1.0
TEMPLATE = """
	A simple quiklist is menu for quick launching program.
	To add items to menu, simple edit the file <i>.quiklist</i> in your home directory (one command per line).
	The line is directly appended to the quick launch command.Please make sure command is correct otherwise program will not launch and will not show icon.

Author: Abhijeet Kasurde 
Email : godfatherabhi@gmail.com
	"""

CONF_FILE = os.getenv("HOME") + "/.quiklist"

def menuitem_response(w, cmd): 
	if cmd == "_refresh":
		newmenu = build_menu() 
		ind.set_menu(newmenu)
	elif cmd == "_about":
		msgbox = gtk.MessageDialog(None, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		msgbox.set_markup("<b>Quiklist v%s</b>" % ver)
		msgbox.format_secondary_markup(TEMPLATE)
		msgbox.run()
		msgbox.destroy() 
	else:
		#print "Opening ", cmd 
		try:
			Popen([cmd])	
		except Exception as e:
			dialog = gtk.MessageDialog(None, 0, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK)
		        dialog.set_markup("<b>Quiklist Error</b>")
			dialog.format_secondary_markup("Unable to start %s \nError: %s" %(cmd, e))
			dialog.set_default_response(gtk.RESPONSE_CLOSE) 
			dialog.run() 
			dialog.destroy() 
			#print "Can't open ", cmd, " due to ", e

def create_menu_image(menuname, imagename):
	image = gtk.Image() 
	image.set_from_icon_name(imagename,24)
	item = gtk.ImageMenuItem()
	item.set_label(menuname.title())
	item.set_image(image)
	item.set_always_show_image(True)
	return item 

def create_conf_file():
	try:
		cmdfile = open(CONF_FILE,"w")
		cmdfile.write("gnome-terminal")
		cmdfile.close()
		
		msgbox = gtk.MessageDialog(None, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		msgbox.set_markup("<b>Quiklist<b>")
		msgbox.format_secondary_markup("%s created successfully" %(CONF_FILE))
		msgbox.run()
		msgbox.destroy() 
		
	except:
		sys.exit("Error Occured in writing to %s " %(CONF_FILE))

def build_menu():
	#create menu
	menu = gtk.Menu() 
	#read ~/.quiklist for command 
	
	if not os.path.exists(CONF_FILE):
		dialog = gtk.MessageDialog(None, 0, gtk.MESSAGE_ERROR, gtk.BUTTONS_NONE, "Message here")
		dialog.add_button(gtk.STOCK_YES, gtk.RESPONSE_YES)
		dialog.add_button(gtk.STOCK_NO, gtk.RESPONSE_NO)
		dialog.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
		dialog.set_markup("<b>Error</b>")
		dialog.format_secondary_markup("Error in reading %s\nPlease see instructions for installation" %CONF_FILE)

		response = dialog.run() 
		if response == gtk.RESPONSE_YES:
			print "Yes"
			create_conf_file()
			dialog.destroy() 
		elif response == gtk.RESPONSE_NO:
			print "No"
			sys.exit(1)
			dialog.destroy() 
		else:
			dialog.destroy() 
			sys.exit(1)

	cmdfile = open(CONF_FILE,"r").read()
	cmdlist = cmdfile.split("\n")

	while "" in cmdlist:
		cmdlist.remove("") 

	for buf in cmdlist:
		buf = buf.rstrip()
		#print buf
		menu_items = create_menu_image(buf, buf) 
		menu.append(menu_items) 
		menu_items.connect("activate", menuitem_response, buf)
		menu_items.show() 
	
	separator = gtk.SeparatorMenuItem()
	separator.show()
	menu.append(separator)

	menu_items = create_menu_image("Refresh", "gtk-refresh")
	menu.append(menu_items)
	menu_items.connect("activate", menuitem_response, "_refresh")
	menu_items.show() 

	menu_items = create_menu_image("About", "gtk-about")
	menu.append(menu_items)
	menu_items.connect("activate", menuitem_response, "_about")
	menu_items.show() 

	return menu 
	

if __name__ == "__main__": 
	ind = appindicator.Indicator("quiklist", "nautilus", appindicator.CATEGORY_APPLICATION_STATUS) 
	ind.set_status(appindicator.STATUS_ACTIVE) 
	ind.set_attention_icon("gksu.png")

	quiklist_menu = build_menu()
	ind.set_menu(quiklist_menu)

	try:
		gtk.main() 
	except KeyboardInterrupt as e:
		sys.exit("Ctrl+C pressed")
